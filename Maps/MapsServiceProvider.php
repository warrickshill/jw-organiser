<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 11/02/2018
 * Time: 22:02
 */

namespace Maps;

use Illuminate\Support\ServiceProvider;
use Maps\Repositories\Eloquent\AssignmentsRepository;
use Maps\Repositories\Eloquent\MapsRepository;
use Maps\Repositories\Eloquent\UsersRepository;
use Maps\Repositories\Interfaces\AssignmentsInterface;
use Maps\Repositories\Interfaces\MapsInterface;
use Maps\Repositories\Interfaces\UsersInterface;

class MapsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/Http/routes.php');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MapsInterface::class, MapsRepository::class);
        $this->app->singleton(UsersInterface::class, UsersRepository::class);
        $this->app->singleton(AssignmentsInterface::class, AssignmentsRepository::class);
    }
}
