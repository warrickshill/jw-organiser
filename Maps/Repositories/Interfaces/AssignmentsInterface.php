<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 17:52
 */

namespace Maps\Repositories\Interfaces;

use Maps\SDOModels\Assignment;
use SDO\Base\TypeCollection;

/**
 * Interface AssignmentsInterface
 * @package Maps\Repositories\Interfaces
 */
interface AssignmentsInterface
{
    /**
     * @return TypeCollection
     */
    public function all(): TypeCollection;

    /**
     * @param Assignment $assignment
     * @return Assignment
     */
    public function save(Assignment $assignment): Assignment;
}