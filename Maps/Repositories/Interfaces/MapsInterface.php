<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 17:52
 */

namespace Maps\Repositories\Interfaces;


use Maps\SDOModels\Map;
use SDO\Base\TypeCollection;

/**
 * Interface MapsInterface
 * @package Maps\Repositories\Interfaces
 */
interface MapsInterface
{
    /**
     * @return TypeCollection
     */
    public function all(): TypeCollection;

    /**
     * @param $id
     * @return Map
     */
    public function findOrFail($id): Map;

    /**
     * @param Map $map
     * @return Map
     */
    public function save(Map $map): Map;
}