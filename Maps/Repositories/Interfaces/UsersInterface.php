<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 17:52
 */

namespace Maps\Repositories\Interfaces;


use Maps\SDOModels\Map;
use Maps\SDOModels\User;
use SDO\Base\TypeCollection;

/**
 * Interface MapsInterface
 * @package Maps\Repositories\Interfaces
 */
interface UsersInterface
{
    /**
     * @return TypeCollection
     */
    public function all(): TypeCollection;

    /**
     * @param int $id
     * @return User
     */
    public function get(int $id): User;

    /**
     * @param User $user
     * @return User
     */
    public function save(User $user): User;
}