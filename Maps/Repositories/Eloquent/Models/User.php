<?php

namespace Maps\Repositories\Eloquent\Models;

class User extends \App\User
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assignments()
    {
        return $this->hasMany(Assignments::class, 'user_id', 'id');
    }
}
