<?php

namespace Maps\Repositories\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;

class HousingLists extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'HousingLists';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Addresses::class, 'housing_list_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function map()
    {
        return $this->belongsTo(Maps::class,'map_id', 'id');
    }

    /**
     * @param $value
     */
    public function setPostcodeAttribute($value)
    {
        $postcode = strtoupper($value);
        $postcode = preg_replace('~[^A-Z0-9]~', '', $postcode);
        $postcode = preg_replace("/(.*)(.{3})/", "$1 $2", $postcode, 1);

        $this->attributes['postcode'] = $postcode;
    }
}
