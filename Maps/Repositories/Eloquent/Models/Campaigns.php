<?php

namespace Maps\Repositories\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;

class Campaigns extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Campaigns';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_start', 'date_end'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assignments()
    {
        return $this->hasMany(Assignments::class, 'campaign_id', 'id');
    }
}
