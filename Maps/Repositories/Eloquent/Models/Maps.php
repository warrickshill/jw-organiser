<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 16:56
 */

namespace Maps\Repositories\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;

class Maps extends Model
{
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['last_worked', 'next_work'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Maps';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function housingLists()
    {
        return $this->hasMany(HousingLists::class, 'map_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assignments()
    {
        return $this->hasMany(Assignments::class, 'map_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastWorker()
    {
        return $this->hasOne(User::class, 'id', 'last_worker')
            ->withDefault(function () {
                return new User();
            });
    }

    public function getAssignmentAttribute()
    {
        return $this->assignments
            ->first(function ($assignment, $key) {

                return $assignment->date_in == null;
            }, []);
    }

}
