<?php

namespace Maps\Repositories\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;

class Addresses extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['no_or_name', 'chunk_id', 'road', 'order', 'dnc_status', 'dnc_checked'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['dnc_checked'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['dnc_status' => 'boolean'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function HousingList()
    {
        return $this->belongsTo(HousingLists::class, 'housing_list_id', 'id');
    }
}
