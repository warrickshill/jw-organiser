<?php

namespace Maps\Repositories\Eloquent\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Assignments extends Model
{
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_out', 'date_in'];

    protected $fillable = ['user_id', 'map_id', 'date_out', 'notes', 'date_in'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Assignments';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(Campaigns::class, 'campaign_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function map()
    {
        return $this->belongsTo(Maps::class, 'map_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
