<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 17:51
 */

namespace Maps\Repositories\Eloquent;

use Maps\Repositories\Eloquent\Models\Maps;
use Maps\Repositories\Eloquent\Models\User;
use Maps\Repositories\Interfaces\UsersInterface;
use Maps\SDOModels\Map;
use SDO\Base\TypeCollection;

class UsersRepository implements UsersInterface
{
    /**
     * @return TypeCollection
     */
    public function all(): TypeCollection
    {
        $data = User::all();

        return new TypeCollection($data, \Maps\SDOModels\User::class);
    }

    /**
     * @param int $id
     * @return \Maps\SDOModels\User
     */
    public function get(int $id): \Maps\SDOModels\User
    {
        $user = User::findOrFail($id);

        return new \Maps\SDOModels\User($user);
    }

    /**
     * @param \Maps\SDOModels\User $user
     * @return \Maps\SDOModels\User
     */
    public function save(\Maps\SDOModels\User $user): \Maps\SDOModels\User
    {
        $eloquent = User::findOrNew($user->id);
        $eloquent->fill($user->toArray());

        $eloquent->save();

        return new \Maps\SDOModels\User($eloquent);
    }


}