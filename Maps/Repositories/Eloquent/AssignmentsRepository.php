<?php
/**
 * Created by PhpStorm.
 * User: warrick
 * Date: 04/03/2018
 * Time: 23:39
 */

namespace Maps\Repositories\Eloquent;

use Carbon\Carbon;
use Maps\Repositories\Eloquent\Models\Assignments;
use Maps\Repositories\Interfaces\AssignmentsInterface;
use Maps\SDOModels\Assignment;
use SDO\Base\TypeCollection;

class AssignmentsRepository implements AssignmentsInterface
{
    /**
     * @return TypeCollection
     */
    public function all(): TypeCollection
    {
        // TODO: Implement all() method.
    }

    /**
     * @param Assignment $assignment
     * @return Assignment
     */
    public function save(Assignment $assignment): Assignment
    {
        $eloquent = Assignments::with('user')
            ->findOrNew($assignment->id);
        $open = empty($eloquent->date_in);
        $eloquent->fill($assignment->toArray());

        $eloquent->save();

        if ($open && !empty($eloquent->date_in)) {
            $map = $eloquent->map;
            $map->last_worker = $eloquent->user_id;
            $map->last_worked = $eloquent->date_in;
            $map->next_work = Carbon::now()->addMonth();
            $map->save();
        }
//dd($eloquent);
        return new Assignment($eloquent);
    }

}