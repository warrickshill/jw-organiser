<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 17:51
 */

namespace Maps\Repositories\Eloquent;

use Maps\Repositories\Eloquent\Models\Maps;
use Maps\Repositories\Interfaces\MapsInterface;
use Maps\SDOModels\Map;
use SDO\Base\TypeCollection;

class MapsRepository implements MapsInterface
{
    /**
     * @return TypeCollection
     */
    public function all(): TypeCollection
    {
        $data = Maps::orderBy('number', 'asc')
            ->with(['lastWorker'])
            ->get();
        
        return new TypeCollection($data, Map::class);
    }

    /**
     * @param $id
     * @return Map
     */
    public function findOrFail($id): Map
    {
        $data = Maps::with(['lastWorker'])
            ->findOrFail($id);

        return new Map($data);
    }

    /**
     * @param Map $map
     * @return Map
     */
    public function save(Map $map): Map
    {
        $eloquent = Maps::findOrNew($map->id);
        $eloquent->fill($map->toArray());

        $eloquent->save();

        return new Map($eloquent);
    }
}