<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 18:58
 */

namespace Maps\SDOCollections;

use Maps\SDOModels\Address;
use SDO\Base\TypeCollection;

class AddressCollection extends TypeCollection
{
    public function __construct($items = [])
    {
        parent::__construct($items, Address::class);
    }
}