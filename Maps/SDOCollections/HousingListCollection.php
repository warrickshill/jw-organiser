<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 18:58
 */

namespace Maps\SDOCollections;

use Maps\SDOModels\HousingList;
use SDO\Base\TypeCollection;

class HousingListCollection extends TypeCollection
{
    public function __construct($items = [])
    {
        parent::__construct($items, HousingList::class);
    }
}