<?php

Route::middleware('api')
    ->prefix('api')
    ->namespace('Maps\Http\Controllers')
    ->group(function () {

// Maps
        Route::prefix('maps')
            ->group(function () {
                Route::get('', 'MapController@all');

                Route::get('{id}', 'MapController@get');
            });

// Users
        Route::prefix('users')
            ->group(function () {
                Route::get('', 'UserController@all');

                Route::get('{id}', 'UserController@get');
            });

// Assignments
        Route::prefix('assignments')
            ->group(function () {
                Route::get('', 'AssignmentController@all');

                Route::post('{id}', 'AssignmentController@post');
            });
    });