<?php
/**
 * Created by PhpStorm.
 * User: warrick
 * Date: 04/03/2018
 * Time: 23:34
 */

namespace Maps\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Maps\Repositories\Interfaces\AssignmentsInterface;
use Maps\SDOModels\Assignment;

class AssignmentController
{
    /**
     * @var AssignmentsInterface
     */
    private $assignments;

    /**
     * AssignmentController constructor.
     * @param AssignmentsInterface $assignments
     */
    public function __construct(AssignmentsInterface $assignments)
    {
        $this->assignments = $assignments;
    }

    public function post(Request $request)
    {
        $input = $request->all();

        $assignment = new Assignment($input);

        return $this->assignments->save($assignment);
    }
}