<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 22:32
 */

namespace Maps\Http\Controllers;

use App\Http\Controllers\Controller;
use Maps\Repositories\Interfaces\UsersInterface;

class UserController extends Controller
{

    /**
     * @var UsersInterface
     */
    private $users;

    public function __construct(UsersInterface $users)
    {
        $this->users = $users;
    }


    public function all()
    {
        return $this->users->all();
    }

    public function get($id)
    {
        return $this->users->get($id);
    }
}