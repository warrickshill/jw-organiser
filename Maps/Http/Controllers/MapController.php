<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 22:32
 */

namespace Maps\Http\Controllers;

use App\Http\Controllers\Controller;
use Maps\Repositories\Interfaces\MapsInterface;

class MapController extends Controller
{

    /**
     * @var MapsInterface
     */
    private $maps;

    /**
     * MapController constructor.
     * @param MapsInterface $maps
     */
    public function __construct(MapsInterface $maps)
    {
        $this->maps = $maps;
    }


    public function all()
    {
        return $this->maps->all();
    }

    public function get($id)
    {
        return $this->maps->findOrFail($id);
    }
}