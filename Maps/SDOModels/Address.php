<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 22:22
 */

namespace Maps\SDOModels;

use SDO\Base\Model;
use SDO\Types\IsBool;
use SDO\Types\IsCarbonOrNull;
use SDO\Types\IsInt;
use SDO\Types\IsIntOrNull;
use SDO\Types\SafeString;

class Address extends Model
{
    protected $casts = [
        'id' => IsIntOrNull::class,
        'housing_list_id' => IsInt::class,
        'no_or_name' => SafeString::class,
        'road' => SafeString::class,
        'dnc' => IsBool::class,
        'dnc_date' => IsCarbonOrNull::class,
        'order' => IsInt::class,
        'housingList' => HousingList::class,
    ];
}