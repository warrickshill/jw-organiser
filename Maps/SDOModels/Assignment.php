<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 21:41
 */

namespace Maps\SDOModels;

use SDO\Base\Model;
use SDO\Types\IsBool;
use SDO\Types\IsCarbon;
use SDO\Types\IsCarbonOrNull;
use SDO\Types\IsInt;
use SDO\Types\IsIntOrNull;
use SDO\Types\SafeString;

class Assignment extends Model
{
    protected $casts = [
        'id' => IsIntOrNull::class,
        'map_id' => IsInt::class,
        'user_id' => IsInt::class,
        'campaign_id' => IsIntOrNull::class,
        'partial' => IsBool::class,
        'notes' => SafeString::class,
        'emailed' => IsBool::class,
        'date_out' => IsCarbon::class,
        'date_in' => IsCarbonOrNull::class,
        'user' => User::class,
        'map' => Map::class,
    ];

    protected $optionalEloquentAttributes = [
        'user',
    ];
}