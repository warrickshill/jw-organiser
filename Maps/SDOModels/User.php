<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 21:35
 */

namespace Maps\SDOModels;


use SDO\Base\Model;
use SDO\Types\Email;
use SDO\Types\IsBool;
use SDO\Types\IsCarbon;
use SDO\Types\IsIntOrNull;
use SDO\Types\SafeString;

class User extends Model
{
    protected $casts = [
        'id' => IsIntOrNull::class,
        'name' => SafeString::class,
        'email' => Email::class,
        'created_at' => IsCarbon::class,
        'updated_at' => IsCarbon::class,
        'removed' => IsBool::class,
    ];
}