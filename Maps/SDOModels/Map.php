<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 16/02/2018
 * Time: 21:00
 */

namespace Maps\SDOModels;

use Maps\SDOCollections\AssignmentCollection;
use Maps\SDOCollections\HousingListCollection;
use SDO\Base\Model;
use SDO\Types\IsCarbon;
use SDO\Types\IsInt;
use SDO\Types\IsIntOrNull;
use SDO\Types\SafeString;

class Map extends Model
{
    protected $casts = [
        'id' => IsIntOrNull::class,
        'number' => IsInt::class,
        'name' => SafeString::class,
        'area' => SafeString::class,
        'type' => IsInt::class,
        'last_worker' => User::class,
        'last_worked' => IsCarbon::class,
        'next_work' => IsCarbon::class,
        'housingLists' => HousingListCollection::class,
        'lastWorker' => User::class,
        'assignments' => AssignmentCollection::class,
        'assignment' => Assignment::class,
    ];

    protected $optionalEloquentAttributes = [
        'assignment',
    ];
}