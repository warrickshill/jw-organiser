<?php
/**
 * Created by PhpStorm.
 * User: whill
 * Date: 17/02/2018
 * Time: 21:25
 */

namespace Maps\SDOModels;


use Maps\SDOCollections\AddressCollection;
use SDO\Base\Model;
use SDO\Types\IsInt;
use SDO\Types\IsIntOrNull;
use SDO\Types\SafeString;

class HousingList extends Model
{
    protected $casts = [
        'id' => IsIntOrNull::class,
        'map_id' => IsInt::class,
        'name' => SafeString::class,
        'extra_info' => SafeString::class,
        'postcode' => SafeString::class,
        'addresses' => AddressCollection::class,
    ];
}