import {combineReducers} from "redux";
import Maps from './Maps';
import Users from './Users';

export const rootReducer = combineReducers({
    maps: Maps,
    users: Users
});