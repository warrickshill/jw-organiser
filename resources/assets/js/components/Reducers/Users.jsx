export default function (store = [], action) {
    switch (action.type) {
        case 'FETCHED_USERS':
            return action.data;
        default:
            return store;
    }
}