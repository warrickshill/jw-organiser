export default function (store = [], action) {
    switch (action.type) {
        case 'FETCHED_MAPS':
            return action.data;
        case 'FETCHED_MAP':
            const newState = [...store];
            const foundIndex = newState.findIndex(x => x.id == action.data.id);
            newState[foundIndex] = action.data;
            return newState;
        default:
            return store;
    }
}