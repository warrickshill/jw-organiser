import {store} from './../store';

export function getMaps() {
    axios.get('/api/maps')
        .then((res) => {
            store.dispatch({
                type: 'FETCHED_MAPS',
                data: res.data
            });
        })
        .catch((err) => {
            console.log(err)
        });
}

export function getMap(id) {
    axios.get('/api/maps/' + id)
        .then((res) => {
            store.dispatch({
                type: 'FETCHED_MAP',
                data: res.data
            });
        })
        .catch((err) => {
            console.log(err)
        });
}

export function getUsers() {
    axios.get('/api/users')
        .then((res) => {
            store.dispatch({
                type: 'FETCHED_USERS',
                data: res.data
            });
        })
        .catch((err) => {
            console.log(err)
        });
}

export function postAssignment(assignment) {
    axios.post('/api/assignments/' + assignment.id, assignment)
        .then((res) => {
            getMap(assignment.map_id);
        })
        .catch((err) => {
            console.log(err)
        });
}