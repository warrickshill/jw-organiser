import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Tile extends Component {

    render() {
        const {name, area, type, number, id} = this.props;
        return (
            <article className={"map-tile t-" + type}>
                <Link to={"/map/" + id}>
                    <div className="map-tile__header">
                        {number}
                    </div>
                    <div className="map-tile__body">
                        <div className="map-tile__body__name">
                            {name}
                        </div>
                        <div className="map-tile__body__area">
                            {area}
                        </div>
                    </div>
                </Link>
            </article>
        );
    }
}

export default Tile;