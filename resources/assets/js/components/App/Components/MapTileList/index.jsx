import React, {Component} from 'react';
import Tile from '../MapTile';

class TileList extends Component {


    render() {
        const tiles = this.props.maps.map((map, i) => {
            return (
                <div className="map-tile-list__item" key={map.id}>
                    <Tile {...map}/>
                </div>);
        });

        return (
            <div className="map-tile-list">
                {tiles}
            </div>
        );
    }
}

export default TileList;