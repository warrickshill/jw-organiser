import {connect} from 'react-redux';
import TileList from '../../Components/MapTileList';

const getMaps = (maps) => {
    return maps.filter((map) => {
        return map.assignment === undefined;
    }).sort((a, b) => {
        return new Date(b.next_work.date) - new Date(a.next_work.date);
    });
};

const mapStateToProps = state => {
    return {
        maps: getMaps(state.maps),
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

const Unassigned = connect(
    mapStateToProps,
    mapDispatchToProps
)(TileList);

export default Unassigned