import {connect} from 'react-redux';
import TileList from '../../Components/MapTileList';

const mapStateToProps = state => {
    return {
        maps: state.maps,
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

const AllMaps = connect(
    mapStateToProps,
    mapDispatchToProps
)(TileList);

export default AllMaps