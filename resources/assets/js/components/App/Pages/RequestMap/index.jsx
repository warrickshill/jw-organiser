import {connect} from 'react-redux';
import TileList from '../../Components/MapTileList';

const getMaps = (maps) => {
    const types = {};
    return maps.filter((map) => {
        return map.assignment === undefined;
    }).sort((a, b) => {
        return new Date(a.next_work.date) - new Date(b.next_work.date);
    }).filter((map) => {
        const {type} = map;
        if (types[type] === undefined) {
            types[type] = 0;
        }
        types[type]++;
        return types[type] <= 2;
    });
};

const mapStateToProps = state => {
    return {
        maps: getMaps(state.maps),
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

const RequestMap = connect(
    mapStateToProps,
    mapDispatchToProps
)(TileList);

export default RequestMap