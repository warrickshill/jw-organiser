import {connect} from 'react-redux';
import TileList from '../../Components/MapTileList';

const getMaps = (maps) => {
    return maps.filter((map) => {
        return map.assignment !== undefined;
    }).sort((a, b) => {
        return new Date(a.assignment.date_out.date) - new Date(b.assignment.date_out.date);
    });
};

const mapStateToProps = state => {
    return {
        maps: getMaps(state.maps),
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

const Assigned = connect(
    mapStateToProps,
    mapDispatchToProps
)(TileList);

export default Assigned