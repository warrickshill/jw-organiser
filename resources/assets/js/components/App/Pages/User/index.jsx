import {connect} from 'react-redux';
import TileList from '../../Components/MapTileList';

const getMaps = (maps, id) => {
    return maps.filter((map) => {
        return map.assignment !== undefined
            && map.assignment.user_id !== undefined
            && map.assignment.user_id == id;
    });
};

const getUser = (users, id) => {
    return users.find((user) => {
        return user.id == id;
    });
};

const mapStateToProps = (state, params) => {
    return {
        maps: getMaps(state.maps, params.match.params.id),
        user: getUser(state.users, params.match.params.id)
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

const User = connect(
    mapStateToProps,
    mapDispatchToProps
)(TileList);

export default User