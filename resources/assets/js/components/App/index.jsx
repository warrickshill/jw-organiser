import React, {Component} from 'react';
import {Link, Route, Switch} from 'react-router-dom';
import AllMaps from './Pages/AllMaps';
import Assigned from './Pages/Assigned';
import Unassigned from './Pages/Unassigned';
import User from './Pages/User';
import RequestMap from './Pages/RequestMap';

class App extends Component {
    render() {
        return (
            <div style={{height: "100%"}}>
                <div className="nav">
                    <Link to="/">All Maps</Link>
                    <Link to="/unassigned">Unassigned</Link>
                    <Link to="/request">Request a Map</Link>
                    <Link to="/assigned">Assigned</Link>
                </div>
                <div className="main">
                <Switch>
                    <Route path="/users/:id" component={User}/>
                    <Route path="/unassigned" component={Unassigned}/>
                    <Route path="/request" component={RequestMap}/>
                    <Route path="/assigned" component={Assigned}/>
                    <Route path="/" component={AllMaps}/>
                </Switch>
                </div>
            </div>
        );
    }
}

export default App;